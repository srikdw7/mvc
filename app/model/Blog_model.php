<?php 
    class Blog_model {
        private $dbh;
        private $stmt;
        public function __construct() {
            //data source name
            $dsn = "mysql:host=127.0.0.1;dbname=mvc";
            try {
                $this->dbh = new PDO($dsn, "root", "");
            } catch(PDOException $e) {
                die($e->getMessage());
            }
        }
        public function getAllBlog() {
            // query data
            $this->stmt = $this->dbh->prepare("SELECT * FROM blog");
            // eksekusi query
            $this->stmt->execute();
            // mengembalikan respon hasil query berupa array associative
            return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
     }
?>