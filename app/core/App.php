<?php
    class app {
        protected $controller = "Home";
        protected $method = "index";
        protected $params = [];

        public function __construct() {
            $url = $this->parseURL();

            // setup controller
            if(isset($url[0]) && file_exists('../app/controllers/'. $url[0].'.php')) {
            // file_exists digunakan untuk mengecek apakah controller yang diminta user tersedia atau tidak (filenya)
                $this->controller = $url[0];
                unset($url[0]);
                // unset digunakan untuk menghapus elemen controller pada array
            }
            require_once '../app/controllers/'. $this->controller.'.php';
            $this->controller = new $this->controller;

            // setup method
            if(isset($url[1])) {
                if(method_exists($this->controller, $url[1])) {
                // method_exists() digunakan untuk mengecek apakah aplikasi itu memlilki method sesuai dengan data yang kita dapat dari url
                    $this->method= $url[1];
                    unset($url[1]);
                    // unset() ini digunakan untuk set nilai dan juga menghapus elemen   
                }
            }

            // setup params/data
            if(!empty($url)) {
                $this->params = array_values($url);
                // array_values() digunakan untuk mengecek apabila url mengandung data
            }

            // jalankan controller dan method, serta kirim data
            call_user_func_array([$this->controller, $this->method], $this->params);
        }

        public function parseURL() {
            if(isset($_GET['url'])) {
                $url = rtrim($_GET['url'],"/");
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                return $url;
            }
        }
    }
?>