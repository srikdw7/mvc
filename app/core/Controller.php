<?php 
    // class Controller ini akan dijadikan sebagai parent dari controller - controller yang ada
    class Controller {
        // method view() memiliki 2 parameter yaitu alamat_view dan data yang akan dikirim ke view (jika ada)
        public function view($view, $data = []) {
          require_once "../app/view/$view.php";
      }

        public function model($model) {
          require_once "../app/model/$model.php";
          return new $model;
      }
    }
?>